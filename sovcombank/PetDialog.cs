﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace sovcombank
{
    public partial class PetDialog : Form
    {
        List<String> list;
        String date;
        String typePet;
        int count;
        public PetDialog(List<String> source)
        {

            InitializeComponent();
            this.list = source;
            comboBox1.DataSource = list;
        }

        private void button1_Click(object sender, EventArgs e)
        {            
                YesButton();
        }
         private void YesButton()
         {
            
            DialogResult = DialogResult.OK;
            date=dateTimePicker1.Value.ToLongDateString();
            typePet = comboBox1.SelectedItem.ToString();
            count = (int)numericUpDown1.Value;
         }

        private void button2_Click(object sender, EventArgs e)
        {
            NoButton();
        }
        private void NoButton()
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        public String getPet() { return typePet; }
        public String getDate() { return date; }
        public int getCount() { return count; }
        public void setPet(String name)
        {
            for (int i = 0; i < comboBox1.Items.Count; i++)
            {
                if (comboBox1.Items[i].ToString().Equals(name)) { comboBox1.SelectedIndex = i; break; }
            }
        }
        public void setDate(string date) { dateTimePicker1.Value = DateTime.Parse(date); }
        public void setCount(string count) { numericUpDown1.Value = Int32.Parse(count); }

    }
}
