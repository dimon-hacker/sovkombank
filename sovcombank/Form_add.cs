﻿using System;
using System.Windows.Forms;

namespace sovcombank
{
    public partial class Form_add : Form
    {
        private String value;
        public Form_add()
        {
            InitializeComponent();
        }
        public Form_add(String value)
        {
            InitializeComponent();
            textBox1.Text = value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            YesButton();
            
        }
        private void YesButton() {
            DialogResult = DialogResult.OK;
            value = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NoButton();
        }
        private void NoButton() {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        internal string GetValue()
        {
            return value;
        }
        

        

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter)) YesButton();
            else if (e.KeyCode.Equals(Keys.Escape)) NoButton();
        }
    }
}
