﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Drawing.Printing;
using Microsoft.Office.Interop.Excel;
using System.Globalization;

namespace sovcombank
{
    public partial class Form2 : Form
    {
        String dbfilename= "pets.sqlite.db";
        private SQLiteConnection connection;
        private SQLiteCommand command;
        public Form2()
        {
            InitializeComponent();
            CreateDBAndConnect();
            LoadDataBase();
        }

        
        private void CreateDBAndConnect() {
            if (!File.Exists(dbfilename))
            {
                SQLiteConnection.CreateFile(dbfilename);
            }
            try
            {
                connection = new SQLiteConnection("Data source = " + dbfilename + "; version=3;");
                connection.Open();
                command = new SQLiteCommand();
                command.Connection = connection;
                command.CommandText = "CREATE TABLE IF NOT EXISTS pets (id INTEGER PRIMARY KEY AUTOINCREMENT, Type TEXT)";
                command.ExecuteNonQuery();
                command.CommandText = "CREATE TABLE IF NOT EXISTS pets_list (id INTEGER PRIMARY KEY AUTOINCREMENT,Date TEXT, Pet TEXT, Count INTEGER)";
                command.ExecuteNonQuery();

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadDataBase() {
            System.Data.DataTable table = new System.Data.DataTable();
            String query="Select * from pets";
            SQLiteDataAdapter adpter = new SQLiteDataAdapter(query, connection);
            adpter.Fill(table);
            dataGridView1.DataSource = table;
            query = "Select * from pets_list";
            adpter = new SQLiteDataAdapter(query, connection);
            table = new System.Data.DataTable();
            adpter.Fill(table);
            dataGridView2.DataSource = table;
        }

        private String selectFromDb(String val) {
            String query = String.Format("Select Type from pets where id = {0}", val);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            SQLiteDataReader reader = command.ExecuteReader();
            String tmp = "";
            while (reader.Read())
            {
                tmp = String.Format("{0}", reader[0]);
            }
            return tmp;
        }
        private String selectPetFromDb(String val)
        {
            String query = String.Format("Select * from pets where Type = '{0}'", val);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            SQLiteDataReader reader = command.ExecuteReader();
            String tmp = "";
            while (reader.Read())
            {
                tmp = String.Format("{0}", reader[0]);
            }
            return tmp;
        }
        private String[] selectFromPets(String val)
        {
            String query = String.Format("Select Date,Pet,Count from pets_list where id = {0}", val);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            SQLiteDataReader reader = command.ExecuteReader();
            string[] tmp = new string[3];
            while (reader.Read())
            {
                tmp[0] = String.Format("{0}", reader[0]);
                tmp[1] = String.Format("{0}", reader[1]);
                tmp[2] = String.Format("{0}", reader[2]);
            }
            return tmp;
        }
        private void addTodb(String name)
        {
            String query = String.Format("Insert into pets (Type) values ('{0}')",name);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
        private void addTodb(String date,String name,int count)
        {
            String query = String.Format("Insert into pets_list (Date, Pet,Count) values ('{0}','{1}',{2})", date,name,count);
            
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
        private void editIndb(String name,String index)
        {
            String query = String.Format("Update  pets set Type= '{0}' where id={1}", name,index);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
        private void editInPets(string index,string date, string pet, int count)
        {
            string query = String.Format("Update  pets_list set Date= '{1}', Pet='{2}', Count={3} where id={0}",index, date, pet, count);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
        private void removeFromDb(String index) {
            String query = String.Format("Delete from pets where id = {0}", index);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            addButton();
        }

        private void addButton()
        {
            Form_add fa = new Form_add();
            fa.ShowDialog();
            if (fa.DialogResult == DialogResult.OK)
            {
                String val = fa.GetValue();
                if (val.Equals("")) return;
                addTodb(val);
                LoadDataBase();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            EditButton();
        }

        private void EditButton() {
            if (dataGridView1.Rows.Count == 0) return;
            int index = dataGridView1.CurrentRow.Index;
            String indexVal = dataGridView1.Rows[index].Cells[0].Value.ToString();
            String typeOfAnimal = selectFromDb(indexVal);
            Form_add form = new Form_add(typeOfAnimal);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                String val = form.GetValue();
                if (val.Equals("")) return;
                editIndb(val,indexVal);
            }
            LoadDataBase();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            RemoveButton();
        }
        private void RemoveButton() {

            if (dataGridView1.Rows.Count == 0) return;
            int index = dataGridView1.CurrentRow.Index;
            String indexVal = dataGridView1.Rows[index].Cells[0].Value.ToString();
            removeFromDb(indexVal);
            LoadDataBase();
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        
    private void otchet(object sender, EventArgs e)
        {
            
            DialogResult result = MessageBox.Show("Вывести на печать?", "Подтверждение",MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                printDialog1.AllowSomePages = true;
                printDialog1.ShowHelp = true;
                printDialog1.Document = printDocument1;
                result = printDialog1.ShowDialog();
                if (result == DialogResult.OK) printDocument1.Print();
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show(TextToPrint());
            }
    }
        private string TextToPrint() {
            StringBuilder sb = new StringBuilder();
            sb.Append("Date".PadRight(30, ' ') + "Type".PadRight(30, ' ') + "Count");
            sb.Append("\n");
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                sb.Append(dataGridView2[1, i].Value.ToString().PadRight(20,' '));
                sb.Append(dataGridView2[2, i].Value.ToString().PadRight(30,' '));
                sb.Append(dataGridView2[3, i].Value.ToString());
                sb.Append("\n");
            }
            return sb.ToString();
        }
        private void document_PrintPage(object sender,PrintPageEventArgs e)
        {
            System.Drawing.Font printFont = new System.Drawing.Font("Arial", 16, FontStyle.Regular);
            e.Graphics.DrawString(TextToPrint(), printFont, Brushes.Black, 10, 10);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AddToPetList();
        }
        private List<String> getPetTypeList() {
            List<String> list = new List<string>();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                list.Add(dataGridView1[1, i].Value.ToString());
            }
            return list;
        }
        private void AddToPetList() {
            List<String> list=getPetTypeList();
            PetDialog petdialog = new PetDialog(list);
            petdialog.ShowDialog();
            if (petdialog.DialogResult == DialogResult.OK)
            {
                String date = petdialog.getDate();
                String pet = petdialog.getPet();
                int count = petdialog.getCount();
                addTodb(date, pet, count);

            }
            LoadDataBase();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            EditPetList();
        }

        private void EditPetList()
        {
            if (dataGridView2.Rows.Count == 0) return;
            int index = dataGridView2.CurrentRow.Index;
            String indexVal = dataGridView2.Rows[index].Cells[0].Value.ToString();
            String[] values=selectFromPets(indexVal);
            List<String> list = getPetTypeList();
            PetDialog form = new PetDialog(list);
            form.setDate(values[0]);
            form.setPet(values[1]);
            form.setCount(values[2]);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                editInPets(indexVal,form.getDate(), form.getPet(), form.getCount());
            }
            LoadDataBase();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RemoveFromPetList();
        }

        private void RemoveFromPetList()
        {
            if (dataGridView2.Rows.Count == 0) return;
            int index = dataGridView2.CurrentRow.Index;
            String indexVal = dataGridView2.Rows[index].Cells[0].Value.ToString();
            removeFromPets(indexVal);
            LoadDataBase();
        }

        private void removeFromPets(string indexVal)
        {
            String query = String.Format("Delete from pets_list where id = {0}", indexVal);
            command = new SQLiteCommand();
            command.Connection = connection;
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
        private void fillFromExcel() {
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            if (excelApp == null)
            {
                Console.WriteLine("Excel is not installed!!");
                return;
            }
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog1.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;
            try
            {
                Workbook excelBook = excelApp.Workbooks.Open(filename);
                Worksheet excelSheet = excelBook.Sheets[1];
                Range excelRange = excelSheet.UsedRange;
                int rows = excelRange.Rows.Count;
                int cols = excelRange.Columns.Count;

                for (int i = 1; i <= rows; i++)
                {
                    string date = "";
                    string pet = "";
                    int count = 1;
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    date = excelRange.Cells[i, 1].Value2.ToString();
                    pet = excelRange.Cells[i, 2].Value2.ToString();
                    bool res = Int32.TryParse(excelRange.Cells[i, 3].Value2.ToString(), out count);
                    if (res)
                    {
                        addTodb(date, pet, count);
                        if (selectPetFromDb(pet).Equals("")) addTodb(pet);
                    }
                }
                LoadDataBase();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excelApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            }

        }

        private void openFileExcel(object sender, EventArgs e)
        {
            fillFromExcel();
        }
    }
}
